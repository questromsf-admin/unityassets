/*-----
    TOC:
        Section 1: Header/Nav and footer Related
        Section 2: Content Related (Global)
            2b: Hide objects when clicked outside of it (add any new modals, menus, etc to this function)
            2c: Swipe events
            2d: On-off toggle btn
            2e: Card toggle
            2f: 2 line ellipse
            2g: table toggle icon
            2h: scrollToBottom of div with overflow, messages functionality on mobile
            2i: follow/following toggle
            2j: cookie reader
        Section 3: Form Related
        Section 4: Post Box
        Section 5: Carousel
        Section 6: Simple modal
        Section 7: Multiselect filter
        Section 8: jQuery UI Daterange picker
            8b: Timepicker
        Section 9: On-boarding
        Section 10: Login images
        Section 11: Text editor
        Section 12: Tooltip
    -----*/
var desktopMQuery = "(min-width: 1150px)";
var mobileMQuery = "(max-width: 1149px)";

$(document).ready(function() {
  var toggleSpeed = 300;
  var firstLoadMobile = true;
  /*-----
      Section 1: Header/Nav and footer
      ------*/

  ////////////////////////////// Sidebar nav ///////////////////////////////

  $('nav#sidebar').on('click', '.hamburger-menu', function(event) {
    event.preventDefault();
    var windowWidth = Math.max(document.documentElement.clientWidth,
      window.innerWidth || 0)
    if ($('nav#sidebar').hasClass('menuClosed')) {
      $('nav#sidebar, section#mainContent, footer#colophon').addClass(
        'menuOpen');
      $('nav#sidebar, section#mainContent, footer#colophon').removeClass(
        'menuClosed');
    } else if ($('nav#sidebar').hasClass('menuOpen')) {
      $('nav#sidebar, section#mainContent, footer#colophon').addClass(
        'menuClosed');
      $('nav#sidebar, section#mainContent, footer#colophon').removeClass(
        'menuOpen');
    } else if (windowWidth < 1050) {
      $('nav#sidebar, section#mainContent, footer#colophon').addClass(
        'menuOpen');
    } else {
      $('nav#sidebar, section#mainContent, footer#colophon').addClass(
        'menuClosed');
    }

    $('nav#sidebar li.hasChildren.expanded').each(function() {
      $(this).removeClass('expanded');
      $(this).find('ul').hide();
    });
  });


  $('nav#sidebar').on('click', 'li.hasChildren > a, li.search', function(
    event) {
    event.preventDefault();
    var newThis = $(this);
    if (!$(this).hasClass('search')) {
      newThis = $(this).parent('li.hasChildren');
    }
    var toggle = 0;
    if ($('nav#sidebar').attr('class') == 'menuOpen') {
      toggle = 1;
    }
    var windowWidth = Math.max(document.documentElement.clientWidth,
      window.innerWidth || 0)
    if ((windowWidth < 1050 && !$('nav#sidebar').hasClass('menuOpen')) ||
      $('nav#sidebar').hasClass('menuClosed')) {
      $('nav#sidebar .hamburger-menu').click();
    } else {
      $('nav#sidebar').removeClass('menuClosed');
      $('nav#sidebar').addClass('menuOpen');
      toggle = 1;
    }
    // newThis.addClass('notMe');
    // $('li.hasChildren.expanded:not(.notMe)').find('ul').slideUp();
    // $('li.hasChildren.expanded:not(.notMe)').removeClass('expanded');
    // newThis.removeClass('notMe');
    newThis.toggleClass('expanded');
    if (toggle == 1) {
      newThis.find('ul').slideToggle();
    } else {
      newThis.find('ul').fadeToggle();
    }
    $('div.sidebar-item-tooltip').remove();
    if (newThis.hasClass('search')) {
      $('li.search input').focus();
    }
    // if (newThis.hasClass('expanded') && !newThis.hasClass('search')) {
    //   $('nav#sidebar').animate({
    //     scrollTop: newThis.position().top
    //   });
    // }
  });

  if ($('nav#sidebar li.hasChildren.expanded').length) {
    var windowWidth = Math.max(document.documentElement.clientWidth, window
      .innerWidth || 0)
    if (!(windowWidth < 1050 && !$('nav#sidebar').hasClass('menuOpen'))) {
      $('li.hasChildren.expanded').find('ul').show();
    }
  }

  $(window).on('resize', function() {
    if (!$('nav#sidebar.menuOpen').length) {
      var windowWidth = Math.max(document.documentElement.clientWidth,
        window.innerWidth || 0)
      if ((windowWidth < 1050 && !$('nav#sidebar').hasClass('menuOpen')) ||
        $('nav#sidebar').hasClass('menuClosed')) {
        $('li.hasChildren.expanded').find('ul').hide();
        $('li.hasChildren.expanded').removeClass('expanded');
      }
    }
  });

  //check if keyboard on mobile is activated
  var is_keyboard = false;
  var is_landscape = false;
  var initial_screen_size = window.innerHeight;

  /* Android */
  window.addEventListener("resize", function() {
    is_keyboard = (window.innerHeight < initial_screen_size);
    is_landscape = (screen.height < screen.width);

    updateViews();
  }, false);

  //call function when keyboard is activated on mobile
  function updateViews() {
    // if ($('nav#sidebar li.search input:focus').length) {
    //   $('nav#sidebar').animate({
    //     scrollTop: $('nav#sidebar').prop("scrollHeight")
    //   }, 200);
    // }
  }

  /* iOS */
  $("input").bind("focus blur", function() {
    $('nav#sidebar').scrollTop(10);
    is_keyboard = $('nav#sidebar').scrollTop() > 0;
    $('nav#sidebar').scrollTop(0);
    updateViews();
  });

  $(document).on('scroll', function() {
    if ($('div.sidebar-item-tooltip').length) {
      $('div.sidebar-item-tooltip').remove();
    }
  });
  $('nav#sidebar').on('scroll', function() {
    if ($('div.sidebar-item-tooltip').length) {
      $('div.sidebar-item-tooltip').remove();
    }
  });



  // collapse menu on print click
  $('.printBtn').on('click', function() {
    $('nav#sidebar, section#mainContent, footer#colophon').addClass(
      'menuClosed');
    $('nav#sidebar, section#mainContent, footer#colophon').removeClass(
      'menuOpen');
  });

  $('nav#sidebar li').hover(
    function(e) {
      //handler in
      var y = $(this).position().top;
      var windowWidth = Math.max(document.documentElement.clientWidth,
        window.innerWidth || 0)
      if ((windowWidth < 1050 && !$('nav#sidebar').hasClass('menuOpen')) ||
        $('nav#sidebar').hasClass('menuClosed')) {
        var labelName = $(this).find('span.sidebar-text').text();
        $('section#mainContent').append(
          '<div class="sidebar-item-tooltip" style="top:' + (y + 10) +
          'px;">' + labelName + '</div>')
        if ($(this).children('a').hasClass('active')) {
          $('div.sidebar-item-tooltip').addClass('active');
        }
      }
    },
    function() {
      //handler out
      $('div.sidebar-item-tooltip').remove();
    }
  );


  ////////////////////////////// Sidebar nav ///////////////////////////////

  //toggle menu
  $('.droptoggle').on('click', '.toggle-header', function() {
    $(this).parent('.droptoggle').toggleClass('active');
    $(this).next('ul').slideToggle();
  });

  $('.mobile-select .droptoggle ul li').on('click', 'a', function(event) {
    var tab = $(this).attr('data-href');
    var parent = $(this).closest('.droptoggle');
    $('.mobile-toggle-visibility').hide();
    if (tab) {
      event.preventDefault();
      $(tab).show();
      $(tab).addClass('visible');
    }
    $('.mobile-select .droptoggle ul li a.selected').removeClass(
      'selected');
    $(this).addClass('selected');
    parent.toggleClass('active');
    parent.find('.toggle-header').html($(this).text() +
      ' <i class="lrm lrm-arrow"></i>');
    parent.find('ul').slideToggle(100);
  });

  //delete notifications
  $('.feedMenuItem').on('click', '.remove', function() {
    $(this).parent('.feedMenuItem').slideUp();
  });

  //mark notifications as read
  $('.feedMenuItem').on('click', '.read', function() { /*removed to a page*/ });

  // show and hide the left nav on mobile
  $('nav.mobileSubNav a').on('click', function() {
    $(this).toggleClass('expanded');
    $('div.leftNav').slideToggle(toggleSpeed);
  });

  // show and hide the view account menu
  $('a.view', 'header#masthead').on('click', function(event) {
    if (Modernizr.mq(desktopMQuery)) {
      $(this).siblings('div.dropdown').slideToggle(toggleSpeed);
      $(this).siblings('ul').slideToggle(toggleSpeed);
      event.preventDefault();
    };
  });

  // show and hide the search Menu
  $('a.search', 'header#masthead').on('click', function(event) {
    /*if(Modernizr.mq(desktopMQuery)){*/
    $('li.searchBox').slideToggle(toggleSpeed);
    event.preventDefault();
    /*}*/
  });

  // Toggle the sectionNav (assignment detail)
  $('ul.sectionNav').on('click', 'a', function(e) {
    e.preventDefault();
    var currentTarget = $(this).attr('href');
    $.scrollTo(currentTarget, {
      duration: 1000,
      offset: -(Global.sectionNavHeight() + 20)
    });
  });

  // expand and collapse standard accordion menus
  $('div.accordian div.listHeader', 'section#mainContent').on('click',
    function() {
      $(this).parent('div.accordian').toggleClass('expanded').find(
        '.accordianContent').slideToggle(toggleSpeed);
    });

  // Expand primary one on load
  $('div.accordian.expanded .accordianContent').show();

  // Ensure the link on the list header doesn't trigger the expansion of the accordion
  $('div.listHeader h3 a', 'section#mainContent').on('mousedown click',
    function(e) {
      e.stopPropagation();
    });

  // Ensure the checkbox on the list header doesn't trigger the expansion of the accordion
  $('div.listHeader checkbox', 'section#mainContent').on('mousedown click',
    function(e) {
      e.stopPropagation();
    });

  // Ensure the label on the list header doesn't trigger the expansion of the accordion
  $('div.listHeader label', 'section#mainContent').on('mousedown click',
    function(e) {
      e.stopPropagation();
    });

  // Set the filter toggle
  $('div.filterToggle', 'section#mainContent').on('click', function() {
    $(this).toggleClass("expanded");
    $(this).siblings('ul.filters').slideToggle().toggleClass("expanded");
  });

  // Set the filter toggle
  $('a.actionMenuBtn', 'section#mainContent').on('click', function(event) {
    event.preventDefault();
    $(this).toggleClass("expanded");
    $(this).siblings('ul').slideToggle(300).toggleClass("expanded");
  });

  //basic toggle
  $('#initialGroupSection').on('click', '.basicToggle .toggleHeader',
    function() {
      $(this).next('.toggleContent').slideToggle();
      $(this).toggleClass('expanded');
    });



  // Set rosterList action toggle
  $('ul.rosterList li').on('click', function() {
    var shouldExpand = 1;

    if ($(this).hasClass("expanded")) {
      shouldExpand = 0;
    }

    $('ul.rosterList li').removeClass("expanded");
    $('ul.rosterList li').children('ul').slideUp(150).removeClass(
      "expanded");

    if (shouldExpand == 1) {
      $(this).addClass("expanded");
      $(this).children('ul').slideDown(300).addClass("expanded");
    }
  });

  $('ul.tabbedNav li', 'section#mainContent').on('click', function(e) {
    e.preventDefault();
    var menuUl = $(this).closest('ul');
    var allMenuItems = $(menuUl).find('li');
    var tabbedSections = $(menuUl).siblings('div.tabbedSections').children(
      '.section');
    var currentIndex = $(allMenuItems).index($(this));
    if (Modernizr.mq('(max-width: 767px)')) {
      if (!menuUl.hasClass('showAll')) {
        menuUl.addClass('showAll');
      } else {
        allMenuItems.removeClass('selected');
        $(this).addClass('selected');
        tabbedSections.removeClass('selected');
        $(tabbedSections[currentIndex]).addClass('selected');
        menuUl.removeClass('showAll');
      }
    } else {
      allMenuItems.removeClass('selected');
      $(this).addClass('selected');
      tabbedSections.removeClass('selected');
      $(tabbedSections[currentIndex]).addClass('selected');
    }
  });

  var allPageTabs = $('a', 'nav#pageTab');
  var allPageSections = $('div.tabbedPage', 'section#mainContent');
  allPageTabs.on('click', function(e) {
    e.preventDefault();
    var currentParentLi = $(this).parent('li');
    $('li', 'nav#pageTab').removeClass('selected');
    allPageSections.addClass('hide');
    currentParentLi.addClass('selected');
    var currentIndex = $(allPageTabs).index($(this));
    $(allPageSections[currentIndex]).removeClass('hide');
  });

  $('header#masthead').on('click', 'div.right > ul li:last-child, a.view',
    function(e) {
      e.stopPropagation();
    });

  $('html').on('click', function() {
    if (Modernizr.mq('(min-width: 900px)')) {
      $('a.view', 'header#masthead').siblings('ul').slideUp(toggleSpeed);
    }
  });
  // Hack for ios to allow menu hide click on the html element
  // above to function properly (http://www.quirksmode.org/blog/archives/2010/09/click_event_del.html)
  $('section#mainContent').on('click', function() {});


  //Sticky side nav
  //get default top of stickyNav
  var stickyNav = $("#stickySideNav");
  if (stickyNav.length > 0) {
    var stickyNavPosition = stickyNav.offset();

    function setStickyNav() {
      var stickyNavWidth = $("#stickyNavHeightSet").width();
      var scrollTop = $(window).scrollTop();
      var stickyTop = stickyNavPosition.top;
      var paddingCompensation = 30;
      var marginCompensation = -10;
      if (viewportWidth().width > 992) {
        if ((scrollTop > stickyTop - 10)) {
          stickyNav.css({
            'position': 'fixed',
            'top': '10px',
            'width': stickyNavWidth + paddingCompensation + 'px',
            'margin-left': marginCompensation + 'px'
          });
        } else {
          stickyNav.css({
            'position': 'static',
            'width': stickyNavWidth + paddingCompensation + 'px',
            'margin-left': marginCompensation + 'px'
          });
        }
      } else {
        stickyNav.css({
          'position': 'static',
          'width': '100%',
          'margin': '0 auto'
        });
      }
    }

    setStickyNav();
    $(window).scroll(function() {
      setStickyNav();
    });
    $(window).resize(function() {
      setStickyNav();
    });
  }

  // This code is from http://andylangton.co.uk/articles/javascript/get-viewport-size-javascript/
  // $(window).width() does not return the same width as css media queries so this function is needed
  function viewportWidth() {
    var e = window,
      a = 'inner';
    if (!('innerWidth' in window)) {
      a = 'client';
      e = document.documentElement || document.body;
    }
    return {
      width: e[a + 'Width'],
      height: e[a + 'Height']
    };
  }

  function setFooterPush() {
    var footerHeight = $('footer#colophon').height();
    var footerMargin = parseInt($('footer#colophon').css('margin-top'), 10);
    $('.footerPush').height(footerHeight);
    $('.stickyFooter').css('margin', '0 auto -' + (footerMargin +
      footerHeight) + 'px');
  }

  setFooterPush();

  //on scroll functions for footer and cover-nav (independent of each other)
  $(window).on('scroll resize', function() {
    setFooterPush();
    coverNav = $('nav.cover-nav:not([data-static-nav])');
    if (coverNav.length) {
      var windowTop = $(window).scrollTop();
      var navBottom = coverNav.height();
      if (coverNav.hasClass('navOnly')) {
        navBottom = 150;
      }
      if (windowTop > navBottom) {
        coverNav.addClass('stickToPage');
      } else {
        coverNav.removeClass('stickToPage');
      }
    }
  });

  $('nav.cover-nav').on('click', 'h1', function() {
    if ($(this).parents('.stickToPage').length) {
      $("html, body").animate({
        scrollTop: 0
      }, "slow");
    }
  })

  var footerQuotes = ["If you dream it, you can do it.",
    "Never, never, never give up.",
    "Don’t wait. The time will never be just right.",
    "If not us, who? If not now, when? ",
    "Everything you can imagine is real.",
    "I can, therefore I am.",
    "Turn your wounds into wisdom.",
    "Wherever you go, go with all your heart.",
    "Do what you can, with what you have, where you are.",
    "Hope is a waking dream.",
    "Action is the foundational key to all success.",
    "Do one thing every day that scares you.",
    "You must do the thing you think you cannot do.",
    "Don’t regret the past, just learn from it.",
    "Opportunities don't happen. You create them. ",
    "Believe you can and you’re halfway there.",
    "Live what you love.",
    "The power of imagination makes us infinite.",
    "To be the best, you must be able to handle the worst.",
    "A jug fills drop by drop.",
    "The obstacle is the path.",
    "The best revenge is massive success.",
    "The best way out is always through.",
    "It does not matter how slowly you go as long as you do not stop.",
    "We become what we think about.",
    "An obstacle is often a stepping stone. ",
    "Dream big and dare to fail.",
    "The starting point of all achievement is desire.",
    "Failure is success if we learn from it. ",
    "Success is not a destination. It is a journey.",
    "The power of imagination makes us infinite.",
    "Ambition is not a vice of little people.",
    "Done is better than perfect.",
    "Mistakes are proof that you are trying.",
    "It's kind of fun to do the impossible.",
    "Sometimes the questions are complicated and the answers are simple."
  ];
  var footerAuthor = ["Walt Disney",
    "Winston Churchill",
    "Napoleon Hill",
    "John F. Kennedy",
    "Pablo Picasso",
    "Simone Weil",
    "Oprah Winfrey",
    "Confucius",
    "Theodore Roosevelt",
    "Aristotle",
    "Pablo Picasso",
    "Eleanor Roosevelt",
    "Eleanor Roosevelt",
    "Ben Ipock",
    "Chris Grosser",
    "Theodore Roosevelt",
    " Jo Deurbrouck",
    "John Muir",
    "Wilson Kanadi",
    "Buddha",
    "Zen Proverb",
    "Frank Sinatra",
    "Robert Frost",
    "Confucius",
    "Earl Nightingale",
    "Prescott Bush",
    "Norman Vaughan",
    "Napoleon Hill",
    "Malcolm Forbes",
    "Zig Ziglar",
    "John Muir",
    "Michel de Montaigne",
    "Sheryl Sandberg",
    "Unknown",
    "Walt Disney",
    "Dr. Seuss"
  ];

  function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  var quoteNum = getRandomInt(0, 35);


  $('footer#colophon blockquote').text(footerQuotes[quoteNum]);
  $('footer#colophon cite').text(footerAuthor[quoteNum]);

  /*-----
      Section 2: Content Related (Global)
      ------*/

  // Enable bootstrap popovers
  $('[data-toggle="popover"]').popover();

  // Edit Profile Details
  // removed into page
  // $('.editDetailsBtn').on('click', function(e){
  //     $(this).closest('div.savedDetails').addClass('hide').siblings('div.editDetails').removeClass('hide');
  //     e.preventDefault();
  // });
  // $('.cancelEditDetails').on('click', function(){
  //     $(this).closest('div.editDetails').addClass('hide').siblings('div.savedDetails').removeClass('hide');
  // });

  // Internal page navigation
  $('nav.internalNavigation li a').on('click', function(event) {
    event.preventDefault();
    var href = $(this).attr('href');
    $('.internalNavSection.active').removeClass('active');
    $(href).addClass('active');
    $('.current-page').removeClass('current-page');
    $(this).parent().addClass('current-page');
    $('ul.active').removeClass('active');
    $(this).closest('ul').addClass('active');
  });

  function matchHeight() {
    $('div.row div.heightMatch').matchHeight({
      byRow: true,
      property: 'height',
      target: null,
      remove: false
    });
  }
  matchHeight();

  /*$('.dropmenu > a').on('click', function(){  removed into page }); */

  /////////////////////////////////////////////
  //
  //
  //2b: Hide objects when clicked outside of it (add any new modals, menus, etc to this function)
  //
  /////////////////////////////////////////////


  $(document).on('click', function(event) {
    if (!$(event.target).closest('.hasChildren.active').length) {
      $('.hasChildren.active').children('.subMenu, .dropdown').slideUp();
      $('.hasChildren.active').removeClass('active');
    }
  });


  $('.hasChildren').click(function() {
    if ($('.hasChildren.active').length > 1) {
      $(this).addClass('ignore');
      $('.hasChildren.active:not(.ignore)').children(
        '.subMenu, .dropdown').hide();
      $('.hasChildren.active:not(.ignore)').removeClass('active');
      $(this).removeClass('ignore');
    }
    $('li.searchBox').hide();
  });



  /////////////////////////////////////////////
  //
  //
  //2c: Swipe events
  //
  /////////////////////////////////////////////

  $('.carousel').on('swipeleft', function(event) {
    $(this).find('.next').click();
  })
  $('.carousel').on('swiperight', function(event) {
    $(this).find('.prev').click();
  })

  $('.onoff-switch').on('swipeleft', function() {
    $(this).removeClass('on');
  });

  $('.onoff-switch').on('swiperight', function() {
    $(this).addClass('on');
  });

  /////////////////////////////////////////////
  //
  //
  //2d: On-off toggle btn
  //
  /////////////////////////////////////////////

  $('.onoff-switch').on('click', function() {
    $(this).toggleClass('on');
  });


  /////////////////////////////////////////////
  //
  //
  //2e: Card toggle
  //
  /////////////////////////////////////////////

  $('.card:not(.fakeclass)').on('click',
    '.card-toggle-content:not(.disabled)',
    function(e) {
      e.stopPropagation();
      closePUM();
      $(this).next('.card-content').slideToggle(250, function() {
        $(this).prev().toggleClass('active');
      });
    });


  /////////////////////////////////////////////
  //
  //
  //2f: 2 line ellipse
  //
  /////////////////////////////////////////////

  $(document).on('ready', function() {
    $('.limit-2-lines .limit-content').each(function() {
      var content = $(this);
      var words = content.text().split(" ");
      var newContent = '';
      for (var i = 0; i < words.length; i++) {
        if (words[i].length > 25) {
          words[i] = words[i].slice(0, 20) + '[...]';
        }
        newContent = newContent + words[i] + ' ';
      }
      content.text(newContent);
      var parentHeight = content.parent().height();
      if (content.outerHeight() < parentHeight && content.parents(
          '.news-item').length) {
        content.parent().css('height', 'auto');
        return;
      }
      while (content.outerHeight() > parentHeight) {
        content.html(function(index, text) {
          return text.replace(/\W*\s(\S)*$/, '...');
        });
      }
    })
  });


  /////////////////////////////////////////////
  //
  //
  //2g: table toggle icon
  //
  /////////////////////////////////////////////

  $('.sort-table').on('click', 'th.sortable', function() {
    $(this).parents('.sort-table').find('.sortable.active').removeClass(
      'active');
    $(this).addClass('active');
  });



  /////////////////////////////////////////////
  //
  //
  //2h: scrollToBottom of div with overflow, messages functionality on mobile
  //
  /////////////////////////////////////////////


  $(document).ready(function() {
    $('.scrollToBottom').each(function() {
      $(this).scrollTop($(this)[0].scrollHeight);
    });
  });

  $('.reply-box textarea').on('change keyup', function() {
    if ($(this).val()) {
      $(this).next('a.btn-fill-custom').removeClass('disabled');
    } else {
      $(this).next('a.btn-fill-custom').addClass('disabled');
    }
  });
  //commented because message page didn't work right
  // $('.nav-column').on('click', '.feedMenuItem', function(){
  //     if (($('.nav-column').width() + 16)/$(window).width() == 1 ) {
  //         $('.nav-column').hide();
  //         $('.content-column').show(0,function(){
  //             $('.content-column.scrollToBottom').scrollTop($(this)[0].scrollHeight);
  //         });
  //     }
  // });

  $('.content-column').on('click', '.back-btn', function() {
    $('.content-column').hide();
    $('.nav-column').show();
  });

  //removed into page
  //$(window).on('resize', function(){});


  /////////////////////////////////////////////
  //
  //
  //2i: follow/following toggle
  //
  /////////////////////////////////////////////
  $('ul.tag-list').on('click', 'li a.following, li a.follow', function(e) {
    e.preventDefault();
    $(this).toggleClass('follow');
    $(this).toggleClass('following');
  });
  $('.card').on('click', '.dynamic-follow-btn', function(e) {
    e.preventDefault();
    $(this).toggleClass('follow');
    $(this).toggleClass('following');
  });

  /*
   * 2j
   * Sets our browser cookie to determine whether or not the menu is closed on a given page
   */

  $('#sidebar, .hasChildren a, .back-btn, .card-content a').on('click',
    function() {
      document.cookie = ($('#sidebar').hasClass('menuClosed')) ?
        "apex__unityMenuExpanded=false" : "apex__unityMenuExpanded=true";
    });



  /*-----
      Section 3: Form Related
      ------*/
  var postFeedbackForm = $('div.postFeedback', 'section#mainContent');
  postFeedbackForm.on('focus', 'textarea', function() {
    $(this).parent('div.postFeedback').addClass('expanded');
    $(this).siblings('div.hideBtn').slideDown(300);
  });
  postFeedbackForm.on('click', 'a.cancelBtn', function(e) {
    $(this).closest('div.postFeedback').find('textarea').val("");
    $(this).closest('div.postFeedback').removeClass('expanded');
    if ($(this).closest('div.postFeedback').hasClass('commentBox')) {
      $(this).closest('div.postFeedback').slideUp(300);
    } else {
      $(this).closest('div.hideBtn').slideUp(300);
    }
    e.preventDefault();
  });

  $('a.reply').on('click', function(e) {
    $(this).parent('h5').next('div.postFeedback').slideDown(300).addClass(
      'expanded');
    e.preventDefault();
  });

  $('a.privateReply').on('click', function(e) {
    $(this).parents('h5').next('div.postFeedback').slideDown(300).addClass(
      'expanded');
    $(this).parents('h5').next('div.postFeedback').children(
      '.isPrivateComment').show();
    $(this).parents('.actionMenu').slideToggle(300).toggleClass(
      "expanded");
    e.preventDefault();
  });

  $("div#dropZone").dropzone({
    url: "/file/post"
  });

  $('section#mainContent').on('click', '.radioOption', function() {
    if (!$(this).hasClass('readOnly')) {
      $(this).siblings('.radioOption').removeClass('selected');
      if ($(this).attr('data-option-group')) {
        $('[data-option-group=' + $(this).attr('data-option-group') +
          ']').removeClass('selected');
      }
      $(this).toggleClass('selected');
      var radioInput = $(this).find('input[type="radio"]');
      $(radioInput).prop("checked", !radioInput.prop("checked"));
    }
  });

  $('div.simpleModalBox').on('click', '.radioOption', function(event) {
    if (!$(this).hasClass('readOnly')) {
      event.stopPropagation();
      $(this).siblings('.radioOption').removeClass('selected');
      if ($(this).attr('data-option-group')) {
        $('[data-option-group=' + $(this).attr('data-option-group') +
          ']').removeClass('selected');
      }
      $(this).toggleClass('selected');
      var radioInput = $(this).find('input[type="radio"]');
      $(radioInput).prop("checked", !radioInput.prop("checked"));
    }
  });

  var radios = $('input[type="radio"]:checked');
  $.each(radios, function(index, value) {
    $(value).closest('div').addClass('selected');
  });

  $('div#lrmmodal, div#mainContent').on('click', 'a.chooseFiles', function(
    e) {
    e.preventDefault();
    $('input.fileUpload').click();
  });

  $('div.animateInput input').on('focus', function() {
    $(this).closest('div.animateInput').addClass('animate');
  });

  $('div.animateInput input').on('blur', function() {
    if ($(this).val() == "") {
      $(this).closest('div.animateInput').removeClass('animate');
    }
  });

  $("#forgotPasswordBtn").click(function(event) {
    event.preventDefault();
    $(".loginPanel").slideUp();
    $(".newPasswordPanel").slideDown();
  });

  $('.selectpicker').selectpicker();

});

$('.card-comments').on('click', 'p.older-comments a', function(event) {
  event.preventDefault();
  $(this).parent('p.older-comments').html(
    '<div class="loader"><div class="ispinner gray animating"><div class="ispinner-blade"></div><div class="ispinner-blade"></div><div class="ispinner-blade"></div><div class="ispinner-blade"></div><div class="ispinner-blade"></div><div class="ispinner-blade"></div><div class="ispinner-blade"></div><div class="ispinner-blade"></div><div class="ispinner-blade"></div><div class="ispinner-blade"></div><div class="ispinner-blade"></div><div class="ispinner-blade"></div></div></div>'
  );
});

$('#mainFeed').on('click',
  '.card-poll-choices .view-results, .card-poll-results .change-vote',
  function(event) {
    event.preventDefault();
    $(this).closest('.card').find('.card-poll-choices').toggle();
    $(this).closest('.card').find('.card-poll-results').toggle();
  })

//custom checkboxes
$('.simpleModalBox, section#mainContent, .on-boarding-content').on('click',
  'label.checkbox',
  function(e) {
    if ($(this).hasClass('not-main')) {
      e.preventDefault();
      return;
    }
    $(this).toggleClass('checked');
    var checkBoxes = $(this).find('input[type="checkbox"]');
    $(checkBoxes).prop("checked", !checkBoxes.prop("checked"));
    e.preventDefault();
    e.stopPropagation();
  });


$('.advancedFilter').on('click', '.expandOptions', function(event) {
  event.preventDefault();
  $(this).parents('.advancedFilter').find('.secondary-options').slideToggle(
    function() {
      if ($(this).is(":visible")) {
        $('.expandOptions').text('(-) Less Options');
      } else {
        $('.expandOptions').text('(+) More Options');
      }
    });
});

/*-----
    Section 4: Post Box
    ------*/

$('.post-options').on('click', 'button[data-content-target]', function() {
  var dataTarget = '.' + $(this).attr('data-content-target');
  var dataContent = $(dataTarget).html();
  var postExtend = $('.post-extra-content');
  var placeholderText = 'Welcome, what would you like to share?';

  removeClassFromNotMe($(this), 'button[data-content-target]', 'active');


  if ($(this).hasClass('upload-btn') && !$('.post-upload-content').hasClass(
      'fileAdded')) {
    $('#fileUploader').click();
  } else {
    $(this).toggleClass('active');
    if ($(this).hasClass('active')) {
      postExtend.html(dataContent);
      postExtend.slideDown(300);
    } else {
      postExtend.slideUp(300, 'swing', function() {
        postExtend.html('');
      });
    }
  }
  //removed not needed
  //if ($(this).hasClass('active')) {}

  $('#post-area').attr('placeholder', placeholderText);
  togglePostButton();
});

function removeClassFromNotMe(me, selectorName, className) {
  me.addClass('ignore');
  $(selectorName + ':not(.ignore)').removeClass(className);
  me.removeClass('ignore');
};

$('#fileUploader').change(function() {
  $('#fileUploader').trigger('changeUpload');
  if (this.value) {
    fileNameOnly = this.value.split('fakepath\\').pop();
    $('.post-upload-content').html(
      '<span class="file-path"><i class="lrm lrm-genericfile"></i><span class="file-name">' +
      fileNameOnly +
      '</span></span><a class="remove context-btn"><i class="lrm lrm-remove"></i> Remove</a>'
    );
    $('.post-upload-content').addClass('fileAdded');
    $('.upload-btn').addClass('active');
    $('.post-extra-content').show(300);
    $('#post-area').attr('placeholder',
      'Write something about this file...');
    togglePostButton();
  } else {
    $('.post-upload-content').html('')
    $('.post-upload-content').removeClass('fileAdded');
    $('.upload-btn').removeClass('active');
    $('#post-area').attr('placeholder',
      'Welcome, what would you like to share?');
    $('.post-extra-content').slideUp();
    togglePostButton();
  }
  $('.post-extra-content').html($('.post-upload-content-holder').html());
});

$('.post-box').on('click', '.post-upload-content .remove', function() {
  $(this).parent().html('');
  $('#fileUploader').val('');
  $('.post-upload-content').removeClass('fileAdded');
  $('.upload-btn').removeClass('active');
  $('#post-area').attr('placeholder',
    'Welcome, what would you like to share?');
  $('.post-extra-content').slideUp();
  togglePostButton();
});

$('.post-extra-content').on('click', '.post-poll-content .add-choices',
  function() {
    var choiceCount = $('.post-extra-content .post-poll-content input').length +
      1;
    $(this).before('<label for="choice-' + choiceCount + '">Choice ' +
      choiceCount + '</label><input type="text" name="choice-' +
      choiceCount + '" id="choice-' + choiceCount +
      '" data-field-for-post>');
  });

$('.post-box').on('keyup', '#post-area', function() {
  $('#post-area-heightmatch').text($(this).val());
  //$(this).height($('#post-area-heightmatch').height());
});


$('.post-box').on('change keyup', '[data-field-for-post]', function() {
  togglePostButton();
});

function togglePostButton() {
  var enableButton = true;
  if ($('#palist.post-options').find('.active').hasClass('poll-btn')) {
    var postBoxElem = $('.post-box [data-field-for-post]');
    for (i = 0; i < postBoxElem.length; i++) {
      if (!postBoxElem[i].value.length) {
        enableButton = false;
      }
    }
  } else if ($('#palist.post-options').find('.active').hasClass('link-btn')) {
    var postBoxInpt = $('.post-box input[data-field-for-post]');
    for (i = 0; i < postBoxInpt.length; i++) {
      if (!postBoxInpt[i].value.length) {
        enableButton = false;
      }
    }
  } else if ($('#palist.post-options').find('.active').hasClass('upload-btn')) {
    enableButton = $('.post-box .file-path').length ? true : false;
  } else {
    enableButton = $('.post-box [data-field-for-post]').val().length ? true :
      false;
  }
  if (enableButton) {
    $('#mesbtn').removeClass('disabled');
  } else {
    $('#mesbtn').addClass('disabled');
  }
};

$('.comment-box').on('change keyup', 'textarea', function() {
  if ($(this).val()) {
    $(this).next('a[class^="btn-"]').removeClass('disabled');
  } else {
    $(this).next('a[class^="btn-"]').addClass('disabled');
  }
});

//removed not needed
//$('.post-box').on('click', '.post-btn:not(.disabled)', function(){});


$('.minimum2Char').on('change keyup', function() {
  if ($(this).val().length > 1) {
    $(this).next('a[class^="btn-"]').removeClass('disabled');
  } else {
    $(this).next('a[class^="btn-"]').addClass('disabled');
  }
});

$(document).on('ready', function() {
  var numberOfCarousels = $('.carousel').index() + 1;
  for (var i = 0; i < numberOfCarousels; i++) {
    var carousel = $('.carousel').eq(i);
    var numPanels = carousel.children('.carousel-items').find(
      '.carousel-item').length;
    for (var j = 0; j < numPanels; j++) {
      carousel.find('.carousel-controls .carousel-buttons').append(
        '<a data-href="' + j +
        '" class="carousel-map-button" role="button"></a>');
    }
    setCarouselButton(carousel, carousel.find('.carousel-item.active').index());
  }
});

$('.carousel').on('click', '.prev, .next', function() {
  if ($(this).parent().next('.carousel-items').find('.carousel-item').length >
    1) {
    var direction = 1;
    if ($(this).hasClass('prev')) {
      direction = -1;
    }
    var currentPanel = $(this).parent().next('.carousel-items').children(
      '.carousel-item.active');
    var panelNumber = null;
    if (direction == -1) {
      if (currentPanel.prev('.carousel-item').length) {
        currentPanel.prev('.carousel-item').addClass('active');
      } else {
        $(this).parent().next('.carousel-items').children(
          '.carousel-item:last-child').addClass('active');
      }
    } else {
      if (currentPanel.next('.carousel-item').length) {
        currentPanel.next('.carousel-item').addClass('active');
      } else {
        $(this).parent().next('.carousel-items').children(
          '.carousel-item:first-child').addClass('active');
      }
    }
    currentPanel.removeClass('active');
    panelNumber = $(this).parent().next('.carousel-items').children(
      '.carousel-item.active').index();
    setCarouselButton($(this).closest('.carousel'), panelNumber);
  }
});

$('.carousel').on('click', '.carousel-map-button', function() {
  var panelNumber = $(this).attr('data-href');
  var carousel = $(this).closest('.carousel');
  carousel.find('.carousel-item.active').removeClass('active');
  carousel.find('.carousel-item').eq(panelNumber).addClass('active');
  setCarouselButton(carousel, panelNumber);
});

$('.carousel').on('click', '.minimize', function() {
  var carousel = $(this).closest('.carousel');
  carousel.slideUp();
});



function setCarouselButton(carousel, panelNumber) {
  var currentCarouselPanel = carousel.find('.carousel-items');
  carousel.find('.current-slide-counter').text(parseInt(panelNumber, 10) + 1);
  buttons = carousel.find('.carousel-controls .carousel-buttons');
  buttons.find('.carousel-map-button.active').removeClass('active');
  buttons.find('.carousel-map-button').eq(panelNumber).addClass('active');
  currentCarouselPanel.animate({
    'left': (-((panelNumber - 1) * 100) - 100) + '%'
  }, 250);
}

// removed into page
//$('.card').on('click', '.editPost', function(){});
// removed into page
//$('.comment').on('click', '.editComment', function(event){});

//News functionality

// removed into page
// $(function() {});

$('#shareOnGroupFeedCheckbox').on('click', function() {
  if ($(this).hasClass('checked')) {
    $('#shareOnGroupFeedSelect').slideUp();
  } else {
    $('#shareOnGroupFeedSelect').slideDown();
  }
});

$('#shareOnFeedCheckbox').on('click', function() {
  if (!$(this).hasClass('not-main')) {
    if ($(this).hasClass('checked')) {
      $('#shareOnFeedSelect').slideUp();
    } else {
      $('#shareOnFeedSelect').slideDown();
    }
  }
});

$('#shareOnFeedCheckbox').on('click', function(){
    if (!$(this).hasClass('not-main')) {
        if ($(this).hasClass('checked')) {
            $('#shareOnFeedSelect').slideUp();
        } else {
            $('#shareOnFeedSelect').slideDown();
        }
    }
});


//Create resource toggle

$(function() {
  $('.selectpicker.resourceType').on('change', function(e) {
    var selected = $(this).find("option:selected").val();
    if (selected == 'File/Photo') {
      $('.browseBtn').show();
      $('.browseBtn').prev().attr('placeholder', '');
    } else if (selected == 'Link') {
      $('.browseBtn').hide();
      $('.browseBtn').prev().attr('placeholder', 'http://example.com');
    }
  });
});

//like button animations

$('article#mainFeed').on('click', 'a.card-btn', function() {
  if (!$(this).hasClass('share-btn')) {
    if ($(this).hasClass('active')) {
      decrementBtn($(this));
      $(this).removeClass('active');
    } else {
      incrementBtn($(this));
      $(this).addClass('active');
    }
  }
});

function incrementBtn(element) {
  element.addClass('increment');
  setTimeout(function() {
    element.removeClass('increment');
  }, 250)
}

function decrementBtn(element) {
  element.addClass('decrement');
  setTimeout(function() {
    element.removeClass('decrement');
  }, 250)
}


/*-----
    Section 6: Simple modal
    ------*/

$('a.fsModal').click(function(event) {
  window.scrollTo(0, 0);
  event.preventDefault;
  modal = $(this).attr('data-href');
  $(modal).toggle();
  $('html').addClass('fsModalOpen');
  $(modal).css({
    'top': $(window).scrollTop()
  });
});
$(
  '.blockBtnWrapper, .centerBlockButtons, .user-information, #usersList, #profilefollowers, #profilefollowings, #grManagersHeader, #grManagersHolder, #grMembersHolder, #messagesPageNewMessage, #recepientsHeader, .post-options#palist, .modalContent #newsForm' /*section.mainContent removed because of double invoking*/
).on('click', '.simpleModal', function(event) {
  event.preventDefault;
  modal = $(this).attr('data-href');
  $(modal).toggle();
  $(modal).css({
    'top': $(window).scrollTop()
  });
  if ($(this).hasClass('editScale')) {
    populateEditScale($(this), modal);
  };
  if ($(this).hasClass('modalSetFocus')) {
    $(modal).find('input').select();
    $(modal).find('input').focus();
  };
});
$('.closeModal').click(function(event) {
  event.preventDefault;
  $('html').removeClass('fsModalOpen');
  $(this).closest('.simpleModalBox, .fsModal').toggle();
});
$('.simpleModalBox').on('click', 'label.checkbox-green', function(e) {
  e.preventDefault();
  e.stopPropagation();
  $(this).toggleClass('checked');
  var checkBoxes = $(this).find('input[type="checkbox"]');
  $(checkBoxes).prop("checked", !checkBoxes.prop("checked"));
  if ($(this).hasClass('checked')) {

  }
});
$('label.check-master').on('click', function(e) {
  var parent = $(this).closest('.resourceAccordian');
  var checkChildren = $(parent).find('.check-children');
  if ($(this).hasClass('checked')) {
    $(checkChildren).find('label').addClass('checked');
  } else {
    $(checkChildren).find('label').removeClass('checked');
  }
});



/*-----
    Section 7: Multiselect filter
    ------*/


//on click event for custom multiselect field, set input field size and focus. display autocomplete menu
$('#initialGroupSection, .overflow-filter, .multiselectHolder').on('click',
  '.multiselect',
  function(e) {
    e.stopPropagation();
    var inputField = $(this).children('input.multiselectInput');
    var blockHolderObject = $(this).children('.blockHolder');
    var selectBoxWidth = $(this).width();
    var widthOfBoxes = blockHolderObject.width();
    var setInputSize = selectBoxWidth - widthOfBoxes - 10;
    if (setInputSize < 30) {
      setInputSize = selectBoxWidth;
    }
    inputField.width(setInputSize)
    inputField.focus();
    if ($('#groupsInput').val() && $('#groupsInput').val().trim().length > 0) {
      $('#shareSearch').slideDown(250);
    } else {
      $(this).children('.menuSelect').slideDown(250);
    }
  });

//on keydown event for custom multiselect field, makes sure width of input is always correct size, deletes blocks if input empty and backspace is pressed
$('#initialGroupSection, .overflow-filter, .multiselectHolder').on('keydown',
  '.multiselect',
  function(e) {
    e.stopPropagation();
    var inputField = $(this).children('input.multiselectInput');
    var blockHolderObject = $(this).children('.blockHolder');
    var selectBoxWidth = $(this).width();
    var widthOfBoxes = blockHolderObject.width();
    var setInputSize = selectBoxWidth - widthOfBoxes - 20;
    if (setInputSize < 30) {
      setInputSize = selectBoxWidth;
    }
    inputField.width(setInputSize)
    if (e.keyCode == 8) {
      if (inputField.val() == '') {
        blockHolderObject.children('.block:last-child').remove();
      }
    }
  });

$('#initialGroupSection, .overflow-filter, .multiselectHolder').on('keyup',
  '.multiselect',
  function(e) {
    $(this).children('.menuSelect').slideDown(250);
  });
//delete multiselect boxes when close button is pressed
// $('#initialGroupSection, .overflow-filter, .multiselectHolder').on('click', '.multiselect .delete', function(){
//     $(this).parent('.block').remove();
// });

//delete group when close button is pressed
$('#initialGroupSection, .overflow-filter, .multiselectHolder').on('click',
  '.groupSection > .delete',
  function() {
    $(this).parent('.groupSection').remove();
  });

//expand/collapse dropdowns within multiselect dropdown
$('#initialGroupSection, .overflow-filter, .multiselectHolder').on('click',
  '.multiselect .expandedArrow',
  function(e) {
    e.stopImmediatePropagation();
    $(this).next('ul').slideToggle();
    $(this).children('span').toggleClass('icon-ml_011ChevronDown');
    $(this).children('span').toggleClass('icon-ml_012ChevronUp');
  });

//scroll through list and hit enter to select
var classItemIndex = 0;
window.onkeyup = function(e) {
  var code = e.which;

  //check up and down
  var selector = ".option-" + classItemIndex;
  if (code == 38) {
    do {
      classItemIndex--;
      selector = ".option-" + classItemIndex;
    } while ($(selector).length && !$(selector).is(":visible"));
  }; //down
  if (code == 40) {
    do {
      classItemIndex++;
      selector = ".option-" + classItemIndex;
    } while ($(selector).length && !$(selector).is(":visible"));
  }; //up
  if (!$(selector).length) {
    classItemIndex--;
  };
  if (classItemIndex < 0) {
    classItemIndex = 0;
  };
  selector = ".multiselectInput:focus + .menuSelect .option-" +
    classItemIndex;
  $('.highlighted').removeClass('highlighted');
  $(selector).addClass('highlighted');

  //check enter
  if (code == 13) {
    $('.highlighted').click();
    $('.highlighted').removeClass('highlighted');
    classItemIndex = 0;
  }
};



// hides multiselect autocomplete dropdown when clicked elsewhere
$(document).mousedown(function(e) {
  var focused = $(':focus');
  if ($(focused).hasClass('multiselectInput')) {
    var container = $(focused).closest('.multiselect');
    if (!container.length) {
      return;
    }

    if (!container.is(e.target) // if the target of the click isn't the container...
      && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
      container.children('.menuSelect').slideUp(250);
      $('.highlighted').removeClass('highlighted');
      classItemIndex = 0;
    }
  }

  if (!$(focused).hasClass('multiselectInput')) {
    $('.multiselect').parent('.multiselectHolder').each(function(i) {
      if ($(this).find('.menuSelect').css('display') == 'block' && $(e.target)
        .closest(this).find('.multiselect').length == 0) {
        $(this).find('.menuSelect').slideUp(250);
        $('.highlighted').removeClass('highlighted');
        classItemIndex = 0;
      }
    });
  }
});

$(document).mousedown(function(e) {
  var container = $('.toggleModal');

  if (!$('.toggleModal').length) {
    return;
  }

  if (!container.is(e.target) // if the target of the click isn't the container...
    && container.has(e.target).length === 0) // ... nor a descendant of the container
  {
    container.children('.toggleContent').slideUp(250);
    container.children('.toggleHeader').removeClass('expanded');
  }
});



// hides multiselect autocomplete dropdown when tabbed out of it
$(document).keyup(function(e) {
  var container = $('.multiselect');
  if (!container.length) {
    return;
  }

  if (!container.is(e.target) // if the target of the click isn't the container...
    && container.has(e.target).length === 0) // ... nor a descendant of the container
  {
    container.children('.menuSelect').slideUp(250);
    $('.highlighted').removeClass('highlighted');
    classItemIndex = 0;
  }
});

//add block to field if clicked
// $('#initialGroupSection, .overflow-filter, .multiselectHolder').on('click', '.addItem', function(){
//     if ($('#initialGroupSection').length) {
//         $(this).closest('.multiselect').find('.blockHolder .block').remove(); // single block per group enabled, delete to allow for multiple selections
//     }
//     var content = $(this).text();
//     $(this).closest('.multiselect').find('.blockHolder').append('<div class="block">' + content + ' <button class="delete lrm lrm-remove it-btnDeleteBlock1"></button></div> ');
// });


/*-----
    Section 8: jQuery UI Daterange picker
    ------*/



//jQuery UI daterangepicker
function initializeDateRangeCal(calendarID, startDate, endDate) {
  $(calendarID).datepicker({
    minDate: 0,
    numberOfMonths: [2, 1],
    setDate: new Date(),
    // showOtherMonths: true,
    beforeShowDay: function(date) {
      var date1 = $.datepicker.parseDate($.datepicker._defaults.dateFormat,
        $(startDate).val());
      var date2 = $.datepicker.parseDate($.datepicker._defaults.dateFormat,
        $(endDate).val());
      return [true, date1 && ((date.getTime() == date1.getTime()) || (
          date2 && date >= date1 && date <= date2)) ? "dp-highlight" :
        ""
      ];
    },
    onSelect: function(dateText, inst) {
      var date1 = $.datepicker.parseDate($.datepicker._defaults.dateFormat,
        $(startDate).val());
      var date2 = $.datepicker.parseDate($.datepicker._defaults.dateFormat,
        $(endDate).val());
      var selectedDate = $.datepicker.parseDate($.datepicker._defaults.dateFormat,
        dateText);


      if (!date1 || date2) {
        $(startDate).val(dateText);
        $(endDate).val("");
        $(this).datepicker();
      } else if (selectedDate < date1) {
        $(endDate).val($(startDate).val());
        $(startDate).val(dateText);
        $(this).datepicker();
      } else {
        $(endDate).val(dateText);
        $(this).datepicker();
      }
    }
  });
};

initializeDateRangeCal("#dateRangePicker1", "#inputA1", "#inputB1");

function initializeDateSingleCal(calendarID, dueDate) {
  var defDate = 'present';
  if ($(dueDate).val()) {
    defDate = $(dueDate).val();
  }
  $(calendarID).datepicker({
    inline: true,
    altField: dueDate,
    minDate: new Date(),
    defaultDate: defDate,
  });
};

function initializeAnyDateCal(calendarID, dueDate) {
  var defDate = 'present';
  var dateRangeRightBorder = (new Date()).getFullYear() + 10;
  if ($(dueDate).val()) {
    defDate = $(dueDate).val();
  }
  $(calendarID).datepicker({
    inline: true,
    altField: dueDate,
    changeMonth: true,
    changeYear: true,
    yearRange: "1900:" + dateRangeRightBorder,
    defaultDate: defDate,
  });
};

//initializeDateSingleCal('#dateSinglePicker1', '#singleInput1');    removed , used below

$(document).on('ready', function() {
  $('.singleDatePicker').each(function() {
    var count = $(this).index('.singleDatePicker') + 1;
    if ($(this).hasClass('enablePastDates')) {
      initializeAnyDateCal('#dateSinglePicker' + count, '#singleInput' +
        count);
    } else {
      initializeDateSingleCal('#dateSinglePicker' + count,
        '#singleInput' + count);
    }
  })
  $('.startendDatePicker').each(function() {
    var count = $(this).index('.startendDatePicker') + 1;
    initializeDateRangeCal('#dateRangePicker' + count, '#inputA' +
      count, '#inputB' + count);
  })
});

$('#initialGroupSection, .overflow-filter, .multiselectHolder').on('click',
  '.buttonBar .save[data-calendar-start]',
  function() {
    var startDateInput = $(this).attr('data-calendar-start');
    var endDateInput = $(this).attr('data-calendar-end');
    var output = $(this).attr('data-calendar-target');
    var startDate = $(startDateInput).val();
    var endDate = $(endDateInput).val();
    if (endDate) {
      $(output).text(startDate + ' - ' + endDate);
    }
    if ($(this).closest('.toggleContent').find('.radioOption:first-child').hasClass(
        'selected')) {
      $(output).text('None');
    }
    $(this).closest('.toggleContent').slideToggle();
    $(this).closest('.toggleContent').prev('.toggleHeader').toggleClass(
      'expanded');
  });

$('#initialGroupSection, .overflow-filter, .multiselectHolder').on('click',
  '.buttonBar .save[data-calendar-duedate]',
  function() {
    var duedateDateInput = $(this).attr('data-calendar-duedate');
    var duedateDate = $(duedateDateInput).val();
    var output = $(this).attr('data-calendar-target');
    if (duedateDate) {
      $(output).text(duedateDate);
    }
    if ($(this).closest('.toggleContent').find('.radioOption:first-child').hasClass(
        'selected')) {
      $(output).text('None');
    }
    $(this).closest('.toggleContent').slideToggle();
    $(this).closest('.toggleContent').prev('.toggleHeader').toggleClass(
      'expanded');
  });



$('section#mainContent, div.listHeader, .simpleModalBox').on('click',
  '.singleDatePicker .datePickerText, .startendDatePicker .datePickerText',
  function(e) {
    e.stopPropagation();
    $(this).next('.datepickerUI-wrapper').slideToggle();
  });

$('section#mainContent, div.listHeader, .simpleModalBox').on('click',
  '.singleDatePicker .cancel, .startendDatePicker .cancel',
  function(e) {
    e.stopPropagation();
    $(this).parents('.datepickerUI-wrapper').slideUp();
  });

$('section#mainContent, div.listHeader, .simpleModalBox').on('click',
  '.singleDatePicker .save[data-calendar-duedate]',
  function(e) {
    e.stopPropagation();
    var duedateDateInput = $(this).attr('data-calendar-duedate');
    var duedateDate = $(duedateDateInput).val();
    var output = $(this).attr('data-calendar-target');
    var timePicker = $(this).parents('.singleDatePicker').find('.selectTime');
    var inputHidden = $(output).parent().find('input[type="hidden"]');

    if (timePicker.length) {
      if (timePicker.find('.hour').val()) {
        duedateDate += ' @ ' + timePicker.find('.hour').val() + ':' +
          timePicker.find('.minute').val() + ' ' + timePicker.find('select').val();
      }
    }
    if (duedateDate) {
      $(output).text(duedateDate);
      inputHidden.val(duedateDate.replace('@', ''));
    }
    $(this).parents('.datepickerUI-wrapper').slideUp();
  });


$('section#mainContent, div.listHeader, .simpleModalBox').on('click',
  '.startendDatePicker .save[data-calendar-start]',
  function(e) {
    e.stopPropagation();
    var startDateInput = $(this).attr('data-calendar-start');
    var endDateInput = $(this).attr('data-calendar-end');
    var output = $(this).attr('data-calendar-target');
    var startDate = $(startDateInput).val();
    var endDate = $(endDateInput).val();
    if (endDate) {
      $(output).text(startDate + ' - ' + endDate);
    }
    $(this).parents('.datepickerUI-wrapper').slideUp();
  });

/////////////////////////////////////////////
//
//
//8b: Timepicker
//
/////////////////////////////////////////////

$('.timePicker').timepicker();

function loopMinute(input) {
  if (input.value > 59) {
    input.value = "00";
  }
  if (input.value < 0) {
    input.value = 59;
  }
}

function loopHour(input) {
  if (input.value > 12) {
    input.value = 1;
  }
  if (input.value < 0) {
    input.value = 12;
  }
}

function formatMinute(input) {
  if (input.value.length > 2) {
    input.value = parseInt(input.value, 10);
  }
  if (input.value.length === 1) {
    input.value = "0" + input.value;
  }
  if (input.value.length == 0) {
    input.value = "00";
  }
}

function formatHour(input) {
  if (input.value.length > 2) {
    input.value = parseInt(input.value, 10);
  }
  if (input.value > 12) {
    input.value = 12;
  }
  if (input.value < 1) {
    input.value = 12;
  }
}



/*-----
    Section 9: On-boarding
    ------*/


$('.on-boarding-tabs').on('click', 'a', function(event) {
  event.preventDefault();
  var tab = $(this).attr('data-href');
  $('li.active').removeClass('active');
  $(this).parent('li').addClass('active');
  $('.on-boarding-content > div').hide();
  $(tab).show();
});

$('.on-boarding-content').on('click',
  'a.btn-fill-custom.block-btn-full:not(.simpleModal)',
  function(event) {
    event.preventDefault();
    //var parent = $(this).parents('div[id]');
    //var activeTab = $('li.active');
    //parent.hide();
    //parent.next().show();
    //activeTab.addClass('complete');
    //activeTab.removeClass('active');
    //activeTab.next().addClass('active');
  });

$('.on-boarding-content').on('click', 'div.previousSlide', function(event) {
  event.preventDefault();
  var parent = $(this).parents('div[id]');
  var activeTab = $('li.active');
  parent.hide();
  parent.prev().show();
  activeTab.removeClass('active');
  activeTab.prev().addClass('active');
});

$('.on-boarding-content').on('click',
  'a.btn-fill-clear.block-btn-full:not(.simpleModal)',
  function(event) {
    event.preventDefault();
    //$('.on-boarding-content > div').hide();
    //$('#accept-terms-content').show();
    //$('li.active').removeClass('active');
    //$('.on-boarding-tabs li:last-of-type').addClass('active');
  });



/*-----
    Section 10: Login images
    ------*/

function LoadImage() {
  var bgImage = new Image(),
    imageOject = $('#highResImage');

  bgImage.src = imageOject.attr('data-image');

  bgImage.onload = function() {
    imageOject.css({
      'background-image': 'url("' + bgImage.src + '")',
      'display': 'none'
    });
    imageOject.fadeIn(1000);
  };

}

$(document).ready(function() {
  if ($('#highResImage').length) {
    LoadImage();
  }
});


$('#forgotPasswordBtn').on('click', function() {
  $('#loginMain').fadeOut(200, 'swing', function() {
    $('#forgotPassword').slideDown(400, 'swing');
  });
});
$('#backToLogin').on('click', function() {
  $('#forgotPassword').fadeOut(200, 'swing', function() {
    $('#loginMain').slideDown(400, 'swing');
  });
});

$('.eye-button').on('tap', function() {
  $(this).toggleClass('hover');
  return;
});

$('.eye-button').hover(
  function() {
    $(this).addClass('hover')
  },
  function() {
    $(this).removeClass('hover')
  }
);

///////// On click for testing forgot password process
$('#submitForgotPassword').on('click', function() {
  $('#forgotPassword').fadeOut(200, 'swing', function() {
    $('#checkEmail').slideDown(400, 'swing');
  });
});

$('#checkEmail').on('click', function() {
  $('#checkEmail').fadeOut(200, 'swing', function() {
    $('#changePassword').slideDown(400, 'swing');
  });
});


/*-----
    Section 11: Text editor
------*/
tinymce.init({
  selector: "#textEditor",
  content_css: "/css/editor_styles.css",
  menubar: false,
  plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code',
    'charactercount'
  ],
  toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link',
  setup: function(editor) {
    editor.on('keyup', function(e) {
      var count = this.plugins["charactercount"].getCount();
      var outputField = "#tinymceCharacterCount";
      if (count > 125000) {
        $(outputField).addClass('overLimit');
      } else {
        $(outputField).removeClass('overLimit');
      }
      $(outputField).text(count +
        ' / 125,000 Characters with formating');
    });
  },
});


/* pop ups ellipsis */
var isStopPropagation = false;

$(document).on('click', function(event) {
  if (!isStopPropagation) {
    slideUpAllVisiblePopups($(this));
  }
  if ($(event.target).closest(".search.it-navMastheadOpenSearch").length ==
    0 && $(event.target).closest(".searchBox").length != 1) $('.searchBox')
    .slideUp();
  if ($(event.target).closest(".datepickerUI-wrapper").length == 0 && $(
      event.target).closest('.ui-datepicker-prev.ui-corner-all').length ==
    0 && $(event.target).closest('.ui-datepicker-next.ui-corner-all').length ==
    0) $('.datepickerUI-wrapper').slideUp();
  isStopPropagation = false;
});

$('.hasChildren').click(function() {
  slideUpAllVisiblePopups();
});

function slideUpAllVisiblePopups(currentElement) {
  $('.dropmenu ul:visible').not(currentElement).slideUp();
};

function dropUpMenu(menu) {
  var currentElement = $(menu).next('ul');
  slideUpAllVisiblePopups(currentElement);
  currentElement.slideToggle();
  isStopPropagation = true;
  //window.event.stopPropagation();
};

function dropmenu(elm) {
  dropUpMenu(elm);
}

function closePUM() {
  slideUpAllVisiblePopups();
  $('.searchBox').slideUp();
  $('.hasChildren').removeClass('active');
  $('.hasChildren .subMenu').slideUp();
  $('.datepickerUI-wrapper').slideUp();
}

/* end pop ups ellipsis */

/*-----
    Section 12: Tooltip
------*/
$('.infoTooltip').on('click', 'a.tooltipToggle', function(event) {
  event.preventDefault();
  displayTooltip($(this), '.tooltipContent', event);
});
$('.infoTooltip').on('click', '.tooltipContent .delete', function(event) {
  event.preventDefault();
  hideTooltipOnX('.tooltipContent', '.infoTooltip a.tooltipToggle.active',
    event);
});

//hide tooltips when clicked elsewhere
$(document).on('click', function(event) {
  if (!$(event.target).closest('.compTooltip').length && !$(event.target).closest(
      '.competencyShowcaseRow span.active').length) {
    $('.compTooltip').hide();
    $('.competencyShowcaseRow span.active').removeClass('active');
  }
  if (!$(event.target).closest('.tooltipContent').length && !$(event.target)
    .closest('.infoTooltip a.tooltipToggle.active').length) {
    $('.tooltipContent').hide();
    $('.infoTooltip a.tooltipToggle.active').removeClass('active');
  }
});

$(document).on('scroll', function(event) {
  if (!$(event.target).closest('.compTooltip').length && !$(event.target).closest(
      '.competencyShowcaseRow span.active').length) {
    $('.compTooltip').hide();
    $('.competencyShowcaseRow span.active').removeClass('active');
  }
  if (!$(event.target).closest('.tooltipContent').length && !$(event.target)
    .closest('.infoTooltip a.tooltipToggle.active').length) {
    $('.tooltipContent').hide();
    $('.infoTooltip a.tooltipToggle.active').removeClass('active');
  }
});


function displayTooltip(thisBtn, tooltipClass, event) {
  if (!thisBtn.hasClass('active')) {
    $(tooltipClass).hide();
    $('.competencyShowcaseRow span.active').removeClass('active');
    var tooltip = thisBtn.next(tooltipClass);
    var compPosition = thisBtn.offset();
    var windowTop = $(window).scrollTop();
    var marginTop = parseInt(thisBtn.css('margin-top'), 10);
    tooltip.show();
    tooltip.css({
      'width': '500px'
    });
    var tooltipWidth = tooltip.outerWidth();
    var tooltipHeight = tooltip.outerHeight();
    if ((event.pageX + tooltipWidth) > window.innerWidth) {
      tooltip.css({
        'top': compPosition.top - windowTop + 20,
        'left': compPosition.left - tooltipWidth + 45,
      });
      if ((tooltip.offset().left) < 10) {
        tooltip.css({
          'left': 0,
          'width': 100 + '%',
        });
      }
    } else {
      tooltip.css({
        'top': compPosition.top - windowTop + 20,
        'left': compPosition.left,
      });
    }
    if ((event.pageY + tooltipHeight) > ($(document).scrollTop() + window.innerHeight)) {
      tooltip.css({
        'top': compPosition.top - windowTop - tooltipHeight,
      });
    }
    if (tooltipWidth >= window.innerWidth - 25) {
      tooltip.css({
        'left': 0,
        'width': 100 + '%',
      });
    }
    thisBtn.addClass('active');
  } else {
    thisBtn.removeClass('active');
    $(tooltipClass).hide();
  }
}


function hideTooltipOnX(tooltipClass, tooltipBtn, event) {
  $(tooltipClass).hide();
  $(tooltipBtn).removeClass('active');
}
